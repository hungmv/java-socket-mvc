
package Client;

import Model.user;
import java.io.*;
import java.net.Socket;

/**
 *
 * @author mavanhung
 */

public class client_socket {
     private int port;
     private String host;
     private Socket Socket;
     
     public client_socket(){
         host = "127.0.0.1";
         port = 1997;
     }
     public void openSocket(){
         try {
             Socket = new Socket(host,port);
         } catch (Exception e) {
             e.printStackTrace();
         }
     }
     public void sendUser(user u){
         try {
             ObjectOutputStream oos = new ObjectOutputStream(Socket.getOutputStream());
             oos.writeObject(u);
         } catch (Exception e) {
             e.printStackTrace();
         }
     }
     public String getResult(){
         String res = "";
         try {
             ObjectInputStream ois = new ObjectInputStream(Socket.getInputStream());
             res = (String)ois.readObject();
         } catch (Exception e) {
         }
         return res;
     }
     public void closeConnection(){
         try {
             Socket.close();
         } catch (Exception e) {
         }
     }
}
