
package Controller;

import Model.user;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;


/**
 *
 * @author mavanhung
 */
public class ServerDB {
    private Connection conn;
    
    public ServerDB() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://103.97.126.29:3306/wtrkvbkm_java"
                    + "?useUnicode=true&characterEncoding=utf-8","wtrkvbkm_java","qrFkrlfsu");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public boolean addUser(user u){
        String sql = "INSERT INTO tbluser(id, username, password, address, birthday, sex, description) VALUES(?,?,?,?,?,?,?)";
        try {
            
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, u.getId());
            ps.setString(2, u.getUserName());
            ps.setString(3, u.getPassword());
            ps.setString(4, u.getAddress());
            ps.setDate(5, new Date(u.getBirthday().getTime()));
            ps.setString(6, u.getSex());
            ps.setString(7, u.getDescription());
            ps.executeUpdate();
            return true; // thanh cong!
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
