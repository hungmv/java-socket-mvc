package Controller;

import Model.user;
import View.JFrame;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import javax.swing.JOptionPane;


/**
 *
 * @author mavanhung
 */

public class server_socket {
    private int port;
    private String host;
    private ServerSocket ServerSocket;
    private ArrayList<Socket> list;
    private ServerDB hmv;
    public server_socket() {
        port = 1997;
        //host = "127.0.0.1";
        list = new ArrayList<>();
        hmv = new ServerDB();
        openSocket();
        while (true) {            
            try {
                Socket s = ServerSocket.accept();
                list.add(s);
                execute(s);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void sendResult(String res){
        try {
            ObjectOutputStream oos = new ObjectOutputStream(list.get(list.size()-1).getOutputStream());
            oos.writeObject(res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void execute(Socket s){ //thuc hien them user
        try {
            user u = receiveUser(s);
            if(hmv.addUser(u)){
                sendResult("ok");
                new JFrame().showMessage("Success!");
            } else{
                sendResult("failed");
                new JFrame().showMessage("Failed!");
            }
        } catch (Exception e) {
            sendResult("ok");
            new JFrame().showMessage("Success!");
            e.printStackTrace();
        }
    }

    public void openSocket(){
        try {
            ServerSocket = new ServerSocket(port);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public user receiveUser(Socket s){
        user u= null;
        try {
            ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
            u = (user)ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return u;
    }
    
    
}