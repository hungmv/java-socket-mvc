package Model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author mavanhung
 */
public class user implements Serializable{

    private int id;
    private String userName;
    private String password;
    private String address;
    private Date birthday;
    private String sex;
    private String description;

    public user() {

    }

    public user(int id, String userName, String password, String address, Date birthday, String sex, String description) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.address = address;
        this.birthday = birthday;
        this.sex = sex;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
